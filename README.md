# NE-RPC

North East Remote Programming Conference - the website.

## Setup instructions

* Run `npm install` to get all the dependencies
* Run `npm run server` to start the local Express server
* Run `npm run build` (in a different terminal) to perform a build
* Visit `http://localhost:4000`