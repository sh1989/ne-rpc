{
  "name": "NE-RPC: Back 2 Sk00l",
  "date": "Friday 25th September, 2020",
  "timezone": "BST",
  "tags": {
    "🎓": "Tutorials",
    "🧠": "Revisiting preconceptions",
    "💻": "Upcoming concepts",
    "🧮": "Back to basics",
    "🎤": "Essential people skills",
    "📔": "Principles"
  },
  "sessions": [
    {
      "slotStart": "09:00",
      "qaStart": "09:45",
      "rooms": [
        {
          "session": {
            "title": "Infrastructure as (.NET) code in Pulumi",
            "description": "Historically \"Infrastructure as code\" more often meant \"markup as code\" - requiring walls of XML, json, or YAML. That very rapidly become very hard to understand. My new role has introduced me to Pulumi where the code is (potentially) your favourite language. Not only that but its is platform agnostic. This talk will show how to build a small \"stack\" of services using Pulumi and to deploy same to (at least) two public clouds.",
            "speaker": {
              "firstName": "James",
              "fullName": "James Murphy",
              "bio": "Murph first laid hands to keyboard in 1979 and has been writing code and learning new things ever since. Having worked on pretty much everything he's probably now a server side specialist. He also displays an enthusiasm for automated build pipelines and thinks he may be getting the hang of hip software development concepts like \"test first\" and \"agile\"",
              "tagLine": "Recumbent riding, ale quaffing, programmer",
              "profilePicture": "https://sessionize.com/image?download=James_Murphy.jpg&f=e6213f7d07a538ef9ce9c7cb251350f7,0,0,0,0,11-2bfb-4b8d-b865-40e9e286db46.7585c6ad-5a64-43bf-8ace-d5fb197fb7ef.jpg",
              "socials": [
                { "name": "twitter", "handle": "@recumbent", "url": "https://twitter.com/recumbent" },
                { "name": "blog", "handle": "Blog", "url": "https://blog.murph.me.uk" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/qT0_tNWaEWg?controls=0",
            "tags": ["🎓", "🧠", "💻"]
          }
        },
        {
          "session": {
            "title": "GraphQL Vs. BFF: A Critical Perspective",
            "description": "Imagine an application that has a web and a mobile, IOS and Android, or that your API is consumed by similar frontends from totally different teams. The functionalities they provide are distinct, hence the need for distinct sets of data and functions. You might think that the solution for this is having an “as generic as possible” backend for all UI’s. From my experience, this kind of backend leads to big issues in matters of performance, entangled user experience as well as extra and unnecessary communication for the development teams in order to align and meet their needs. Fortunately, there is a promising set of approaches taking the stage as they are created with the intention to optimize how front-end applications collaborate with back-ends: BFF (Backend-For-Frontend) pattern and GraphQL. Given these two approaches, which one is the right to consider? Join me in a talk where we will discuss the two approaches, underline both their good and bad sides, and determine which you should consider as the backend technology for your frontend application.",
            "speaker": {
              "firstName": "Mihaela-Roxana",
              "fullName":"Mihaela-Roxana Ghidersa",
              "bio": "Coding and teaching represent my passion, learning is my hobby, having an impact in technology is my goal. I am a software developer and technology passionate, I enjoy building and delivering quality, all the while trying to have fun as much as possible.",
              "tagLine": "Software Developer, Centric IT Solutions",
              "profilePicture": "https://sessionize.com/image?f=f301edcab680eab510f1ef4558fe6a80,200,200,1,0,a32c2cd9-55a7-4213-9fe5-28517c414826.jpg",
              "socials": [
                { "name": "twitter", "handle": "@mihaelag12", "url": "https://twitter.com/mihaelag12" },
                { "name": "linkedin", "handle": "LinkedIn", "url": "https://linkedin.com/in/ghidersam/" },
                { "name": "blog", "handle": "Blog", "url": "https://medium.com/@ghidersa.mihaela" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/8pDsRwJ7MW8?controls=0",
            "tags": ["💻"]
          }
        }
      ]
    },
    {
      "slotStart": "10:15",
      "qaStart": "11:00",
      "rooms": [
        {
          "session": {
            "title": "Testing that the code does what the code does",
            "description": "When unit testing goes wrong, what can we do to make it right? You may have noticed this effect, especially amongst the TDD zealots, often in legacy codebase. So many tests, doing so little. And every time I want to make a change, even just to refactor the code, there's loads of test failures! You hear yourself cry out \"these tests are taking too long to fix\". You're wading through treacle. Why is a test failing because I renamed a variable? What we're seeing here is what I call testing that the code does what the code does. Suffice to say I don't find it the best approach to testing. So in this session, let's go back to basics and discuss what unit tests are meant to be. We will develop techniques to get out of this hole, and how to maintain unit tests that empower rather than hinder refactoring. We will focus on observable outcomes not implemented internals. We'll blur the lines with integration testing in our quest to define what a unit is. Mocks, stubs, spies and doubles - let's learn when they should and shouldn't be used.",
            "speaker": {
              "firstName": "Sam",
              "fullName":"Sam Hogarth",
              "bio": "Sam is fascinated with making teams work together better, and keeping up with the ever-changing world of software engineering. He has a decade's worth of experience in highly-regulated environments, across finance, biotech and energy. Whether it's mobile, desktop, web, server or cloud, he has the battle scars. In his spare time, he can run a fine game of Dungeons and Dragons!",
              "tagLine": "Senior Developer, Tesco Bank",
              "profilePicture": "https://sessionize.com/image?f=4b5155a3f11cae472f6dc95cfc5708d5,200,200,1,0,7e-60f1-425d-93dd-c4aa4bf9bb3f.3a81b080-ca95-4efc-b17e-28e331dc2996.jpg",
              "socials": [
                { "name": "twitter", "handle": "@samhogy", "url": "https://twitter.com/samhogy" },
                { "name": "linkedin", "handle": "LinkedIn", "url": "https://www.linkedin.com/in/sam-hogarth/" },
                { "name": "blog", "handle": "Blog", "url": "https://samhogy.co.uk" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/hdKGlgk-34g?controls=0",
            "tags": ["🧠", "🧮", "📔"]
          }
        },
        {
          "session": {
            "title": "Building Modern MicroServices using Event Driven Architectures",
            "description": "MicroServices are a really big deal, possibly one of the most prolific trends in delivering large scale systems right now, but how do we organise them in a way that is sustainable? Orchestration and Choreography are options, but what patterns can we use to facilitate this?",
            "speaker": {
              "firstName": "Jeff",
              "fullName": "Jeff Watkins",
              "bio": "Jeff has over 21 years in the IT industry, having delivered national scale systems in both the public and private sectors. Jeff is interested in technologies and designs that accelerate delivery in the modern, interconnected world.",
              "tagLine": "Chief Engineer and Electronic Musician at AND Digital",
              "profilePicture": "https://sessionize.com/image?f=0c291819f4491c370f29b79e8f86157c,200,200,1,0,73-0dfe-4da7-be3b-58cdb8f14a97.bcc510a9-cb32-44e4-b147-ce6cf14710aa.jpg",
              "socials": [
                { "name": "linkedin", "handle": "LinkedIn", "url": "https://linkedin.com/in/jeff-watkins-5167111/" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/ZubzXgghfi8?controls=0",
            "tags": ["💻"]
          }
        }
      ]
    },
    {
      "slotStart": "11:30",
      "services": [
        {
          "name": "☕ Lean Coffee",
          "description": "You choose the agenda - no prep needed!"
        }
      ]
    },
    {
      "slotStart":"12:15",
      "services": [
        {
          "name": "Lunch",
          "description": "Grab some food, get some fresh air"
        }
      ]
    },
    {
      "slotStart": "14:00",
      "qaStart": "14:45",
      "rooms": [
        {
          "session": {
            "title": "Exploratory: Testing Humanely, Checking Robotically",
            "description": "",
            "speaker": {
              "firstName": "Cian",
              "fullName": "Cian McCormack",
              "bio":"Software Tester, Scrum Master, Rookie Speaker, Ex-Chef, Spice Enthusiast, Collector of Ducks. co-organiser of @newcastleMoT & @GeordieTest",
              "tagLine": "Software Tester",
              "profilePicture": "https://ne-rpc.co.uk/cian.jpg",
              "socials": [
                { "name": "twitter", "handle": "aCyanNinja", "url": "https://twitter.com/aCyanNinja" },
                { "name": "linkedin", "handle": "LinkedIn", "url": "https://www.linkedin.com/in/cian-mccormack-35908a74o" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/OHM3VEyHoLI?controls=0",
            "tags": ["🧠", "📔", "🧮"]
          }
        },
        {
          "session":{
            "title":"From Tables to Documents—Changing Your Database Mindset",
            "description": "Did you grow up on relational databases? Are document databases a mystery to you? This is the session for you! You'll leave this session with an understanding of: How data is modeled in a tabular (relational) database compared to a document database; How terms and concepts in tabular databases relate to terms and concepts in document databases; The three keys ways you need to change the way you think about storing data when using a document database",
            "speaker":{
              "firstName": "Lauren",
              "fullName": "Lauren Schaefer",
              "bio": "Lauren Hayward Schaefer is a developer advocate for MongoDB. She began her career as a software engineer for IBM where she held a variety of roles including full-stack developer, test automation specialist, social media lead, and growth hacking engineer. She is an international speaker who is skilled in taking hard-to-understand topics and making them seem simple. Lauren holds a BS and MS in Computer Science from North Carolina State University and is the co-inventor of thirteen issued United States patents.",
              "tagLine": "Developer Advocate",
              "profilePicture": "https://sessionize.com/image?download=Lauren_Schaefer.JPG&f=7cc90e29cbbc7d4e51d49657a2e6de27,0,0,0,0,1d-3fd6-4d8f-86a3-9e918cc12892.832ab9d1-48b6-4c63-8c52-5841e4a22422.JPG",
              "socials": [
                { "name": "twitter", "handle": "@lauren_schaefer", "url": "https://twitter.com/lauren_schaefer" },
                { "name": "linkedin", "handle": "LinkedIn", "url": "https://linkedin.com/in/laurenjanece/" },
                { "name": "blog", "handle": "Blog", "url": "https://developer.mongodb.com/author/lauren-schaefer" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/j9N1dIVmV48?controls=0",
            "tags": ["🎓", "🧠", "📔"]
          }
        }
      ]
    },
    {
      "slotStart": "15:15",
      "qaStart": "14:00",
      "rooms":[
        {
          "session":{
            "title":"Svelte is fast and makes you faster",
            "description": "Svelte is an up-and-coming JavaScript framework with a focus on speed and simplicity. There's no runtime libraries, and almost no overheads. I've been using Svelte for months on my open-source project, and have never felt more productive. It's time to spread the word. This session is an introductory tutorial, showing how you can embrace Svelte to make your next project faster (in both senses of the word).",
            "speaker":{
              "firstName": "Steven",
              "fullName": "Steven Waterman",
              "bio":"Jack of all trades, Steven's top skill is thinking of side projects and not doing them. He enjoys improvised comedy, hacky bodges that make you want to cry, and cookies.",
              "tagLine": "Software Developer @ Scott Logic, Chaotic Neutral @ Home",
              "profilePicture": "https://sessionize.com/image?f=9870d59e9729f10126a9de0efdcd2d51,200,200,1,0,cf-50c9-4e1a-9aa1-787bb7434f6c.3b96d044-5775-4052-bd14-b397b09c2fcc.png",
              "socials": [
                { "name": "twitter", "handle": "@SteWaterman", "url": "https://twitter.com/SteWaterman" },
                { "name": "linkedin", "handle": "LinkedIn", "url": "https://linkedin.com/in/steven-waterman" },
                { "name": "blog", "handle": "Blog", "url": "https://www.stevenwaterman.uk/" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/P6u0Uv_VxCU?controls=0",
            "tags": ["🎓", "💻"] 
          }
        },
        {
          "session":{
            "title": "It's all about the BaaS, bout the BaaS, bout the BaaS...no trouble",
            "description": "Backend as a Service is regularly seen as a tool for Frontend developers to build apps quickly and with limited knowledge of how Backend systems are built. With this in mind BaaS has appealed to Digital Agencies. But, how many full stack projects could benefit, by delivering value quickly, leveraging BaaS platforms to get started. BaaS groups together the key system components and provides thought out CI/CD processes and the tooling necessary to get going quickly. I'll explore the use of this concept on a recent enterprise scale cloud migration, specifically the use of Firebase as a quick start for a Google Cloud solution.",
            "speaker":{
              "firstName": "Mark",
              "fullName": "Mark Jose",
              "bio": "A technical principal with more than 20 years of software development experience. I have developed software in most languages and would consider myself a “full stack” developer. When I’m not in front of a laptop, you’ll normally find me under the bonnet of a car trying to figure out how I broke it.",
              "tagLine": "Tinkerer of Machines",
              "profilePicture": "https://sessionize.com/image?download=Mark_Jose.jpg&f=d51e9e56a814d718c73c4dcef16baabd,0,0,0,0,ffd5a8ab-90dd-416a-b8f7-1a062aae2052.jpg",
              "socials": [
                { "name": "twitter", "handle": "@markleejose", "url": "https://twitter.com/markleejose" },
                { "name": "linkedin", "handle": "LinkedIn", "url": " linkedin.com/in/markleejose" }
              ]
            },
            "video": "https://www.youtube-nocookie.com/embed/YR0YjX70llM?controls=0",
            "tags": ["🎓", "💻", "📔"]
          }
        }
      ]
    },
    {
      "slotStart": "16:15",
      "services": [
        {
          "name": "🥳 After-School Club",
          "description": "Join us at our after-event social gathering. What have you learned today?"
        }
      ]
    }
  ]
}